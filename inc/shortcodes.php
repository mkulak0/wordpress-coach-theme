<?php

function shortcode_recommendation($atts){
    ob_start();
    ?>
    <button class="accordion"><?php echo $atts['title']; ?></button>
    <div class="panel">
        <p><?php echo $atts['body']; ?></p>
        <p><?php echo $atts['author']; ?></p>
    </div>

    <?php
    $ret = ob_get_contents();
    ob_end_clean();

    return $ret;
}
add_shortcode('recommendation', 'shortcode_recommendation');