<?php
require_once 'inc/shortcodes.php' ;

function parent_theme_styles(){
    wp_enqueue_style('parent-style', get_template_directory_uri().'/style.css');
}
add_action('wp_enqueue_styles', 'parent_theme_styles', 10);

function coach_scripts(){
    wp_enqueue_script('my-script', get_stylesheet_directory_uri().'/script.js', 0, 0, true);
}
add_action('wp_enqueue_scripts', 'coach_scripts');



?>